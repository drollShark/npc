﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{
    public class Main : MonoBehaviour
    {
        NPC george;
        NPC sveta;
        NPC anton;

        Collider[] colliders;

        private void Start()
        {
            george = new NPC(new George());
            sveta = new NPC(new Sveta());
            anton = new NPC(new Anton());

            //Поскольку я не знаю как сделать по иному, делаю так увы это не стратегия (

            colliders = GameObject.FindObjectsOfType<Collider>();

            for (int i = 0; i < colliders.Length; i++)
            {
                colliders[i].gameObject.AddComponent<MouseController>();
            }

        }
        private void Update()
        {
            george.Move();
            sveta.Move();
            anton.Move();
        }
    }

    interface IMovable
    {
        void Move();
    }

    class George : MonoBehaviour, IMovable
    {
        public void Move()
        {

            GameObject george = GameObject.FindGameObjectWithTag("George");

            GameObject pointToRayCast = GameObject.FindGameObjectWithTag("pointToRayCast");


            if (Physics.Raycast(pointToRayCast.transform.position, Vector3.down, 5f))
            {
                if (Physics.Raycast(george.transform.position, Vector3.down, 0.5f))
                {
                    george.GetComponent<Rigidbody>().velocity = (george.transform.right + Vector3.up) * 2.5f;
                }
            }
            else
            {
                george.transform.rotation *= Quaternion.Euler(Vector3.up * 90);
            }
        }
    }

    class Anton : MonoBehaviour, IMovable
    {
        public void Move()
        {

            GameObject anton = GameObject.FindGameObjectWithTag("Anton");

            GameObject pointToRayCastAnton = GameObject.FindGameObjectWithTag("pointToRayCastAnton");

            if (Physics.Raycast(pointToRayCastAnton.transform.position, Vector3.down, 11f))
            {
                anton.transform.Translate(Vector3.right * 4f * Time.deltaTime, Space.Self);
            }
            else
            {
                anton.transform.Rotate(Vector3.up * 90, Space.World);
            }
        }
    }

    class Sveta : MonoBehaviour,IMovable
    {
        public void Move()
        {
            GameObject sveta = GameObject.FindGameObjectWithTag("Sveta");

            GameObject pointToRayCastSveta = GameObject.FindGameObjectWithTag("pointToRayCastSveta");

            if (Physics.Raycast(pointToRayCastSveta.transform.position, Vector3.down, 5f))
            {
                sveta.transform.Translate(Vector3.right * 4f * Time.deltaTime, Space.Self);
            }
            else
            {
                sveta.transform.Rotate(Vector3.up * 90, Space.World);
            }
        }
    }

    class NPC
    {
        public NPC(IMovable mov)
        {
            movable = mov;
        }

        public IMovable movable;
        public void Move()
        {
            movable.Move();
        }
    }

}
